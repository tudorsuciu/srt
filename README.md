**This repository intends to help create a debian package for libsrt**

Secure Reliable Transport (SRT) is a proprietary transport technology that optimizes streaming performance across unpredictable networks, such as the Internet.

You can download libsrt from [SRT Homepage](https://github.com/Haivision/srt).

---

## Get source of libsrt

You can get release 1.3.1 from homepage:
```
wget https://github.com/Haivision/srt/archive/v1.3.1.tar.gz
mv v1.3.1.tar.gz srt_1.3.1.orig.tar.gz
```

You can get debian dsc and debian archive from this repository:
```
wget https://bitbucket.org/tudorsuciu/srt/downloads/srt_1.3.1-1.debian.tar.xz
wget https://bitbucket.org/tudorsuciu/srt/downloads/srt_1.3.1-1.dsc
```

The source of the debian files comes from [pkg-srt Homepage](https://github.com/zmousm/pkg-srt.git).
As this project uses an older version of the lib, this repository was created.

## Prepare the compilation
You need to check that developper packages are installed:
```
sudo apt-get install build-essential fakeroot dpkg-dev
```
## Compile the package
You can now compile with the command:
```
dpkg-source -x srt_1.3.1-1.dsc
dpkg-buildpackage -rfakeroot -b
```

Enjoy!
