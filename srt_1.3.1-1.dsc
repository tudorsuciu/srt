Format: 3.0 (quilt)
Source: srt
Binary: srt-bin, libsrt-dev, libsrt-1
Architecture: any all
Version: 1.3.1-1
Maintainer: Debian Multimedia Maintainers <pkg-multimedia-maintainers@lists.alioth.debian.org>
Uploaders: Sebastian Ramacher <sramacher@debian.org>
Homepage: https://github.com/Haivision/srt
Standards-Version: 3.9.8
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-multimedia/x265.git
Vcs-Git: https://anonscm.debian.org/git/pkg-multimedia/x265.git
Testsuite: autopkgtest
Testsuite-Triggers: ffmpeg
Build-Depends: debhelper (>= 9), tcl, pkg-config, cmake, libssl-dev 
Package-List:
 libsrt-1 deb libs optional arch=any
 libsrt-dev deb libdevel optional arch=any
 srt-bin deb video optional arch=any
Checksums-Sha1:
 38eaf2f6b04ac1e50950547fd47a2302fff08de4 783953 srt_1.3.1.orig.tar.gz
 0730498698be271964546a909ab0fb0d7cbf598c 2512 srt_1.3.1-1.debian.tar.xz
Checksums-Sha256:
 f202801d9e53cd8854fccc1ca010272076c32c318396c8f61fb9a61807c3dbea 783953 srt_1.3.1.orig.tar.gz
 3970255d1e000d2d71fd0e9e2bddc3218d8dec88878c260c0566ee4d1ea0ff1d 2512 srt_1.3.1-1.debian.tar.xz
Files:
 63646b5df4f8d69a32221d17500690a4 783953 srt_1.3.1.orig.tar.gz
 dc8ea3544d2be2495fa95d1cd857ec6c 2512 srt_1.3.1-1.debian.tar.xz

